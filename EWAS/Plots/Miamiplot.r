library(qqman)
source("manhattanneg.r")

layout(matrix(c(1,2), 2, 1, byrow = FALSE))
par(mar=c(0,4,4,2))
manhattan(EHMCresultssub3, chr= "CHR", bp="MAPINFO", p= "Braak_Stage.p", snp="probe", col = c("MediumBlue", "DarkOrange1"),
suggestiveline = -log10(1e-05), genomewideline = -log10(1.36e-07), ylim=c(0,6.1), ylab="")

par(mar=c(2,4,0,2))
manhattanneg(EMCresultssub, chr= "CHR", bp="MAPINFO", p= "Braak_Stage.p", snp="probe", col = c("MediumBlue", "DarkOrange1"),suggestiveline = log10(1e-05), genomewideline = log10(1.36e-07), ylim=c(-8.1,0), axes=FALSE, ylab="")
axis(2, at=(-8:0),labels=FALSE)
text(-1.4e+08, -8:0, labels=c(8,7,6,5,4,3,2,1,0),xpd=TRUE)
text(c(128433992.148542,326637027.218286,465858356.131084,604171588.280183,722434964.500909,817125464.470356,904884979.939945,
993778703.991185,1081975655.9802,1163947461.79508,1234464382.03438,1390019388.39405,1547663085.39354,1599248522.34659,
1636482518.75717,1756448140.96788,1956778275.24743,2148304612.06312,2329452123.27975,2499412782.73709,2656628551.17174,
2804567622.51932,2955599995.1526,3041995564.95412), -0.25,labels=c("1","2","3","4", "5", "6","7","8","9","10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "X", "Y"))
