library(qqman)

#chr muct be numeric

manhattan(data, bp = "MAPINFO", chr= "CHR.1", snp="probe", 
p="p", genomewideline=-log10(5.8E-8), suggestiveline=-log10(1E-5),
chrlabs = c(1:22, "X", "Y"))