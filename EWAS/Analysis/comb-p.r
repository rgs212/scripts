#This will get files ready for comb-p

read.csv("data.csv", header=T, row.names=1) -> data

#merge results with probe info
merge(data, probes, by="row.names") -> data.probes 

#make rownames probes again
rownames(data.probes) <- data.probes[,1] 

#select columns with CHR, MAPINFO x2 and pvalues
data.probes[,c(4,5,5,3)] -> data.comb 

#rename to add chr to numbers (not needed it already "chr1")
paste("chr",data.comb[,1], sep="") -> data.comb[,1] 

#order data by CHR and MAPINFO
data.comb[order(data.comb[,1], data.comb[,2]),] -> data.sort 

#write bed file for comb-p, no header or rownames
write.table(data.sort, "datacomb.bed", col.names=FALSE,row.names=FALSE, sep="\t") 

#Exit R and go to command line
q()

#comb-p pipeline \
#    -c 4 \          # p-values in 4th column
#    --seed 1e-3 \   # require a p-value of 1e-3 to start a region
#    --dist 200      # extend region if find another p-value within this dist
#    -p $OUT_PREFIX \
#    --region-filter-p 0.1 \ # post-filter reported regions
#    --anno mm9 \            # annotate with genome mm9 from UCSC
#    $PVALS                  # sorted BED file with pvals in 4th column

comb-p  pipeline -c 4 --dist 500 --seed 1e-4 -p data datacomb.bed