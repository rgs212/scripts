#Add as many covariants as you need
#Designed for pheno file with rownames to match colnames of data file

model <- function(row, Outcome, CoVar1, CoVar2, CoVar3){
  fit <- try (
     lm(row ~ CoVar1 + CoVar2 + CoVar2 + Outcome),
     silent=TRUE
  )
  if(inherits(fit,'try-error')) return(rep(NA,8))   #Will return 8 columns on NAs if doesn't work
  as.numeric(summary(fit)$coeff[,c(1,2,4)])         #This will return Estimate, SEs and p-value 
}

result  <- {
     CoVar1     <- pheno[colnames(data), 'column for CoVar1']
     CoVar2     <- pheno[colnames(data), 'column for CoVar2']
     CoVar3     <- pheno[colnames(data), 'column for CoVar3']
     Outcome    <- pheno[colnames(data), 'column for Oucome'])
     
     t( apply(data, 1, model, Outcome, CoVar1, CoVar3, CoVar3))
  }
  
c("Intercept.ES", "CoVar1.ES", "CoVar2.ES", "CoVar3.ES", "Outcome.ES", 
"Intercept.ES", "CoVar1.SE", "CoVar2.SE", "CoVar3.SE", "Outcome.SE", 
"Intercept.p", "CoVar1.p", "CoVar2.p", "CoVar3.p", "Outcome.p")->colnames(result)