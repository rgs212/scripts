#functions needed
#read in data with sample headers, probes rows with probe info
read.csv("data.csv", header=T, row.names=1)->data

#ISCA
source("/gpfs/ts0/projects/Research_Project-185144/Bex/Scripts/Pathwayfunctions.r")
read.csv("/gpfs/ts0/projects/Research_Project-185144/Bex/Ref/BackgroundGenes450K_AllProbes_AnnotatedwGOTerms.csv", header=T)->gene_go.450K
read.csv("/gpfs/ts0/projects/Research_Project-185144/Bex/Ref/EPIC_BackgroundGenes_AnnotatedwGOTerms.csv", header=T)->gene_go.epic
read.csv("/gpfs/ts0/projects/Research_Project-185144/Bex/Ref/GOTermNames.csv")->pathwayNames

#Knight
source("/mnt/data1/Bex/Functions/Pathwayfunctions.r")
read.csv("/mnt/data1/Eilis/Projects/References/450K/BackgroundGenes450K_AllProbes_AnnotatedwGOTerms.csv", header=T)->gene_go.450K
read.csv("/mnt/data1/EPIC_reference/EPIC_BackgroundGenes_AnnotatedwGOTerms.csv", header=T)->gene_go.epic
read.csv("/mnt/data1/Eilis/Projects/References/GeneOntology/GOTermNames.csv")->pathwayNames
read.csv("/mnt/data1/Bex/EPICprobeinfosub.csv", header=T, row.names=1)->probes

##To find p and es columns in EMIF
data[,c(length(data)/2,length(data))]->data.sub
colnames(data.sub)<-c("ES", "P")

#Needed if probe info isn't already in data frame
merge(data.sub, probes, by="row.names")->data.sub.probes
res<-data.sub.probes
rownames(data.sub.probes)<-data.sub.probes[,1]

#OR

res<-data

###

res$UCSC_RefGene_Name<-unlist(lapply(res$UCSC_RefGene_Name, uniqueAnno))
res<-res[!res$UCSC_RefGene_Name == "1",]
#res<-res[!res$UCSC_RefGene_Name == "0",]

## Order probes by significance
res[order(res$P),]->res
res[1:1000,]->res.top
index<-rownames(res.top)

# Pull out terms for your test list "index"
gene_test<-table(unlist(strsplit(res[rownames(res) %in% index, "UCSC_RefGene_Name"], "\\;")))
# Pull out terms for your full list
gene_size<-table(unlist(strsplit(res[,"UCSC_RefGene_Name"], "\\;")))
bg_genes<-names(gene_size)
#EPIC = FALSE for 450k
bg_gene_go<-extractGoTerms(backgroundGenes, EPIC = TRUE)

minPathwaySize<-9 ## adjust if required; set to 0 if you wish to analyse all terms regardless of size.
maxPathwaySize<-2000 ## adjust if required ; set to 10000000 if you wish to analyse all terms regardless of size.

termCount<-table(unlist(strsplit(as.character(bg_gene_go[,2]), "\\|")))
terms<-names(termCount)[which(termCount > minPathwaySize & termCount <= maxPathwaySize)]

## create vector that contains the number of probes per gene that were tested
gene_size<-gene_size[names(gene_size) %in% bg_gene_go[,1]]
gene_size<-as.numeric(gene_size)

#gene_test<-gene_test[bg_gene_go[,1]]
gene_test<-gene_test[bg_gene_go[,1]]
gene_test[is.na(gene_test)]<-0

gene_test_ind<-gene_test
gene_test_ind[which(gene_test_ind > 0)]<-1

library(doParallel)
cl<-makeCluster(8) ## adjust the number in the bracklets to use less or more resources depending on your system limitations. 
registerDoParallel(cl)
r<-foreach(i=1:length(terms), .combine=rbind) %dopar%{
        pathwayAnalysis(terms[i], gene_test, gene_size, bg_gene_go)
    }
    colnames(r)<-c("ID", "Name", "Type", "nProbesinPathway", "nGenesinPathway", "nTestListProbesinPathway",  "nTestListGenesinPathway", "P:GenesinTestList", "OR", "P:GeneSize", "Beta:GeneSize","GenesinTestListAndPathway")


r<-as.data.frame(r, row.names = NULL, stringsAsFactors = FALSE)
for(i in c(4:11)){
    mode(r[,i])<-"numeric"
}

stopCluster(cl)
 
p1.thres<-0.05 ## what threshold to define pathways as significantly enriched.
p2.thres<-0.05 ## what threshold to define pathways as no longer significant.

## sort results by order of significance
r<-r[order(r[,8]),]
r<-r[which(r[,8] < p1.thres & r[,9] > 0),]

output<-c()
while(!is.null(r)){
    if(nrow(r) > 1){
### for all terms repeat analysis controlling for most significant terms
        best_term<-vector(length = nrow(bg_gene_go))
        best_term[grep(r[1,1], bg_gene_go[,2])]<-1
        merge.id<-c()
        merge.name<-c()
        remove<-c()
        for(j in 2:nrow(r)){
            pathway<-vector(length = nrow(bg_gene_go))
            pathway[grep(r[j,1], bg_gene_go[,2])]<-1
            ## automatically merge identical pathways
            if(!identical(gene_test, best_term)){
                model<-glm(pathway ~ gene_test + gene_size + best_term)
                if(summary(model)$coefficients["gene_test", "Pr(>|t|)"] > p2.thres){
                    merge.id<-append(merge.id, r[j,1])
                    merge.name<-append(merge.name, r[j,2])
                    remove<-append(remove, j)
                }
            } else {
                merge.id<-append(merge.id, r[j,1])
                merge.name<-append(merge.name, r[j,2])
                remove<-append(remove, j)
            }
        }
        merge.id<-paste(unlist(merge.id), collapse = "|")
        merge.name<-paste(unlist(merge.name), collapse = "|")
        output<-rbind(output, c(r[1,], merge.id, merge.name))
        r<-r[-c(1, remove),]
        } else {
        output<-rbind(output, c(r, "", ""))
        r<-NULL
    }
}


pathwayNames[1]->pathwayNames.1
pathwayNames[2]->pathwayNames.2
cbind(pathwayNames.1,pathwayNames.2)->PN

for (i in 1:nrow(output)){

as.character(PN[as.character(PN$ID) %in% as.character(output[i,1]), "Name"]) -> n

if(identical(n, character(0)) == TRUE){

n <- NA

}
n -> output[i,"Name"]
 
}
       
write.csv(output, "")   ## put appropriate filename between ""
    