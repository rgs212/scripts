#For running sign test for effect sizes going in same direction
#This can be set to whatever you want

#data is your primary analysis
#data2 is analysis to compare to

#Extract top n (1000) significant probes and corresponding probes from 
#other analysis

data[order(data$P),][1:1000,]->data.top

#Make df of primary and secondary analysis
merge(data.top, data2, by="row.names")->data.1.2.top

#Perform binomial test of probes going in same direction. 
#First number is sum of number of probes going in same diection in ES. 
#Second number is total tests

res <- binom.test(nrow(data.1.2.top[data.1.2.top$ES.x > 0 & data.1.2.top$ES.y > 0,]) + 
nrow(data.1.2.top[data.1.2.top$ES.x < 0 & data.1.2.top$ES.y < 0,]), 1000)

res$p.value