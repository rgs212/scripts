
uniqueAnno<-function(row){
if(row != ""){
	return(paste(unique(unlist(strsplit(as.character(row), "\\;"))), collapse = ";"))
	} else {
	return(row)
	}
	}


extractGoTerms<-function(backgroundGenes, EPIC = TRUE){

	if(EPIC){
		gene_go<-gene_go.epic
	} else {
		gene_go<-gene_go.450K
	}
	backgroundGenes<-intersect(bg_genes, gene_go[,1])
	bg_gene_go<-gene_go[match(backgroundGenes, gene_go[,1]),]
	
	return(bg_gene_go)
}

pathwayAnalysis<-function(each, gene_test, gene_size, bg_gene_go, one.sided = TRUE, overrepresentation = TRUE){

		## logistic regression approach
		pathway<-vector(length = nrow(bg_gene_go))
		pathway[grep(each, bg_gene_go[,2])]<-1
		model<-glm(pathway ~ gene_test + gene_size, family=binomial)
		p1<-summary(model)$coefficients[c("gene_test"),c(4)]
		p2<-summary(model)$coefficients[c("gene_size"),c(4)]
		or<-exp(summary(model)$coefficients[c("gene_test"),c(1)])
		beta<-summary(model)$coefficients[c("gene_size"),c(1)]
		if(one.sided & overrepresentation){
			print(paste("Testing for overrepresentation of test genes in ", each, sep = ""))
			if(or > 1){
				p1<-p1/2
			} else {
				p1<-1-(p1/2)
			}
		} else {
			if(one.sided & !overrepresentation){
			print(paste("Testing for underrepresentation of test genes in ", each, sep = ""))
				if(or < 1){
					p1<-p1/2
				} else {
					p1<-1-(p1/2)
				}		
			
			}
		}

		
		return(unlist(c(each, pathwayNames[match(each, pathwayNames[,1]),2:3], sum(gene_size[which(pathway == 1)]), length(which(pathway == 1)), sum(gene_test[which(pathway == 1  & gene_test >= 1)]), length(which(pathway == 1 & gene_test >= 1)), p1,or,p2,beta,paste(bg_gene_go[which(pathway == 1 & gene_test >= 1),1], collapse = "|"))))

}

