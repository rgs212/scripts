#R
library("MatrixEQTL")
setwd("/gpfs/mrc0/projects/Research_Project-MRC164847/Bex/mQTL/MatrixEQTL/Data/")
base.dir = "/gpfs/mrc0/projects/Research_Project-MRC164847/Bex/mQTL/MatrixEQTL/Data/"

#setwd("/mnt/data1/Bex/mQTL/CB/")
#base.dir = "/mnt/data1/Bex/mQTL/CB/"

useModel = modelLINEAR; # modelANOVA or modelLINEAR or modelLINEAR_CROSS

#SNP file is rows as SNPs and columns of samples.
#File must be in 0/1/2 coding
SNP_file_name = "./Genotypes/braak_meta_all_QCd_unrelated_EUR_LBB1LBB2_CBpedrecodetrans.raw"

#Genes file is rows as probes and columns of samples.
expression_file_name = "./Methylation/LBB2LBB1CBEWASnew.txt"

#Covariates file is rows as covariates and columns of samples.
covariates_file_name = "./Covariates/LBB1LBB2covar.txt"
output_file_name = tempfile();

#Significant threshold to save
 pvOutputThreshold = 1e-8;
 errorCovariance = numeric();
 
 print("space set-up")

## Load genotype data
snps = SlicedData$new();
snps$fileDelimiter = " ";       # the space character
snps$fileOmitCharacters = "NA"; # denote missing values;
snps$fileSkipRows = 6;          # six row of column labels
snps$fileSkipColumns = 1;       # one column of row labels
snps$fileSliceSize = 2000;      # read file in pieces of 2,000 rows
snps$LoadFile(SNP_file_name);

print("SNPs done")

## Load gene expression data
gene = SlicedData$new();
gene$fileDelimiter = "\t";      # the TAB character
gene$fileOmitCharacters = "NA"; # denote missing values;
gene$fileSkipRows = 1;          # one row of column labels
gene$fileSkipColumns = 1;       # one column of row labels
gene$fileSliceSize = 2000;      # read file in slices of 2,000 rows
gene$LoadFile(expression_file_name);

print("Meth done")

## Load covariates
cvrt = SlicedData$new();
cvrt$fileDelimiter = "\t";      # the TAB character
cvrt$fileOmitCharacters = "NA"; # denote missing values;
cvrt$fileSkipRows = 1;          # one row of column labels
cvrt$fileSkipColumns = 1;       # one column of row labels
if(length(covariates_file_name)>0) {
cvrt$LoadFile(covariates_file_name);
}

print("Covar done")

# Load in snppos (columns are snp, chr, pos)
snpspos = read.table("./Genotypes/snpsloc.txt", header = TRUE, stringsAsFactors = FALSE);

# Load in genepos (columns are geneid (probe id), chr, s1 and s2)
genepos = read.table("./Methylation/geneloc.txt", header = TRUE, stringsAsFactors = FALSE);

print("snp and genepos loaded")
print("mQTL running")

## Run the analysis

me = Matrix_eQTL_engine(
    snps = snps,
    gene = gene,
    cvrt = cvrt,
    output_file_name = "./Output/outputCBLBB1LBB21emin8.txt",
    pvOutputThreshold = pvOutputThreshold,
    useModel = useModel,
    errorCovariance = errorCovariance,
    verbose = TRUE,
    pvalue.hist = TRUE,
    min.pv.by.genesnp = FALSE,
    noFDRsaveMemory = FALSE);

print("mQTL done")

print(cat('Analysis done in: ', me$time.in.sec, ' seconds', '\n');)
print(cat('Detected eQTLs:', '\n');)
show(me$all$eqtls)

# save results file
save(me, file="./Output/CBLBB1LBB2mqtl.rda")

## Plot the histogram of all p-values
tiff("./Output/CBLBB1LBB2mqtlhistogram.tiff")
plot(me)
dev.off()
