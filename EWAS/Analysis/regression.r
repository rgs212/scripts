#Add as many covariants as you need
#Designed for pheno file with rownames to match colnames of data file

reg.model <- function(row, CoVar1, CoVar2, CoVar3){
  fit <- try (
     lm(row ~ CoVar1 + CoVar2 + CoVar2),
     silent=TRUE
  )
  if(inherits(fit,'try-error')) return(rep(NA,8))   #Will return 8 columns on NAs if doesn't work
  fit$residuals                                     #Will return data regressed for covariates
}

regressed.data  <- {
     CoVar1     <- pheno[colnames(data), 'column for CoVar1']   #Match pheno rownames with data colnames
     CoVar2     <- pheno[colnames(data), 'column for CoVar2']
     CoVar3     <- pheno[colnames(data), 'column for CoVar3']

     t( apply(data, 1, reg.model, CoVar1, CoVar3, CoVar3))
  }
  
