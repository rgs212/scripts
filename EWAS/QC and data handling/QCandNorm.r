##Packages
library(minfi)
library(wateRmelon)

#######Set working Directory
setwd("/mnt/data1/Bex/Meta") #Choose a folder in your folder where things will save to

##Load pheno file
##This file has both MTG and CER in it, can choose to do QC together or separate, we will do them together in this example
pheno<-read.csv("/mnt/data1/Bex/AD/iDATs/Arizona/idat_files/ArizonaPhenoData.csv", header=T, row.names=7)

#Load data
data<-methylumIDAT(rownames(pheno),idatPath="/mnt/data1/Ehsan/meta_CrossCortical/AZ1/IDATS/")

#Extract betas from object
betas(data)->data.betas 

#Data chekcing plots
boxplot(log(methylated(data)), las=2, cex.axis=0.6 , cex=0.6)
boxplot(log(unmethylated(data)), las=2, cex.axis=0.6 , cex=0.6)

m_intensities<-methylated(data)
u_intensities<-unmethylated(data)

M.median<-apply(m_intensities, 2, median)
U.median<-apply(u_intensities, 2, median)

as.data.frame(M.median)->M.median
as.data.frame(U.median)->U.median

merge(M.median, U.median ,by="row.names")->MU.median
rownames(MU.median)<-MU.median[,1]
MU.median[,1]<-NULL

hist(MU.median$M.median, main= "data median signal", xlab="", col=rgb(1,0,0,0.5), xlim=c(0,8000))
hist(MU.median$U.median, add=T,  col=rgb(0,0,1,0.5))
legend(6000,300,c("Methylated", "Unmethylated"), fill=c(rgb(1,0,0,0.5), rgb(0,0,1,0.5)))
abline(v=1000, col="red")

plot(M.median~U.median, data=MU.median, pch=19, cex=0.7, xlab="Median U Intensity", ylab="Median M Inensity", xlim=c(0,8000), ylim=c(0,8000))     
rect(0,0,1000,8000, col=c(rgb(1,0,0,0.5)), border=NA)
rect(1000,0,5000,1000, col=c(rgb(1,0,0,0.5)), border=NA)

#Sex check
read.csv("/mnt/data1/Bex/Adam Data/Xprobes.csv")->Xprobes

Fmd  <- cmdscale(dist(t(exprs(data)[rownames(fData(data)) %in% Xprobes$x,])),2)
merge(pheno, Fmd, by = "row.names", all = TRUE)->pheno.Fmd

plot(V2~V1, data=pheno.Fmd,col=(pheno.Fmd$Gender2))

#PCA
na.omit(data.betas )->data.betas.na

prcomp(t(data.betas.na))->data.betas.na.PCA

data.betas.na.PCA$x->data.betas.na.PCA.out

merge(pheno, data.betas.na.PCA.out, by="row.names")->pheno.PCA
rownames(pheno.PCA)<-pheno.PCA[,1]
pheno.PCA[,1]<-NULL

plot(PC1~PC2, data=pheno.PCA, col=c(pheno.PCA$Gender2))
plot(PC3~PC2, data=pheno.PCA, col=c(pheno.PCA$Region))

#Pfilter and normalisation
pfilter(data,  perc =5)->data.pf

dasen(data.pf)->data.pf.dasen

#Post QC plots
boxplot(log(methylated(DataLBBA.pf.dasen)), las=2, cex.axis=0.6 , cex=0.6)

boxplot(log(unmethylated(DataLBBA.pf.dasen)), las=2, cex.axis=0.6 , cex=0.6)
